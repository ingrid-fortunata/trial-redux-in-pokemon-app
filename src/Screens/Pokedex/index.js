import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Layout from "Components/Layout";

class Pokedex extends Component {
  render() {
    return (
      <div>
        <Layout>
          <div className="home">
            <div className="home__title">Pokemon Favourites</div>

            <div className="home__grid container">
              {this.props.favorit.length === 0
                ? null
                : this.props.favorit.map((pokemon, index) => {
                    return (
                      <div className="home__grid__item" key={index}>
                        <Link
                          className="home__grid__item__content"
                          to={`/pokemon/${pokemon.id}`}
                        >
                          <span>{pokemon?.name}</span>
                        </Link>
                      </div>
                    );
                  })}
            </div>
          </div>
        </Layout>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    favorit: state.favoritPokemon,
  };
};

export default connect(mapStateToProps)(Pokedex);
