import Home from "Screens/Home";
import Pokedex from "Screens/Pokedex";

export const publicRoutes = [
  {
    component: Home,
    path: "/",
    exact: true,
  },
  {
    component: Pokedex,
    path: "/Pokedex",
    exact: true,
  },
];
