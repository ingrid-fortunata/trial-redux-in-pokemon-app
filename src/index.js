import React from "react";
import ReactDOM from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";

import "Styles/Main.scss";
import App from "./App";

//init state
const globalState = {
  favoritPokemon: [],
};

// reducer
const rootReducer = (state = globalState, action) => {
  switch (action.type) {
    case "SET_FAVORIT_POKEMON":
      return {
        ...state,
        favoritPokemon: state.favoritPokemon.concat(action.newValue),
      };
    default:
      break;
  }
  return state;
};

//store
const storeRedux = createStore(rootReducer);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={storeRedux}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
